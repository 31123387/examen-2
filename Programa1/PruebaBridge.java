public class PruebaBridge {
	public static void main(String[] args) {
		Forma cuadradoRojo = new Cuadrado(new Rojo(), "cuadrado");
		Forma circuloAzul = new Circulo(new Azul(), "circulo");

		cuadradoRojo.dibujar();
		circuloAzul.dibujar();
	}//termina el main
}//termina la clase PruebaBridge
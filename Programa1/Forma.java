public abstract class Forma{
	protected Color color;
	protected String figura;

	protected Forma(Color color, String figura){
		this.color = color;
		this.figura = figura;
	}//termina el constructor Forma

	public abstract void dibujar();
}//termina la clase Forma
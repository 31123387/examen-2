public class Circulo extends Forma {

	public Circulo(Color color, String figura){
		super(color,figura);
	}//termina el constructor

	public void dibujar(){
		color.pintaColor(figura);
	}//termina el metodo dibujar
}//termina la clase circulo

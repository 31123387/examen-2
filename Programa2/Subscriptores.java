import java.util.ArrayList;
import java.util.List;

public class Subsciptores {
   private String nombre;
   private String direccion;
   private String correoElectronico;
   private List<Subsciptores> listaSubs;

   // constructor
   public Subsciptores(String nombre,String direccion, String correoElectronico) {
      this.nombre = nombre;
      this.direccion = direccion;
      this.correoElectronico = correoElectronico;
      listaSubs = new ArrayList<Subsciptores>();
   }

   public void add(Subsciptores s) {
      listaSubs.add(s);
   }

   public void remove(Subsciptores s) {
      listaSubs.remove(s);
   }

   public List<Subsciptores> getListaSubs(){
     return listaSubs;
   }

   public String toString(){
      return ("");
   }   
}
public class PizzaNormal implements HornearPizza{

	@Override
	public void hornearPizzaNormal(String tamano, String tipo) {
		System.out.println("Se horneo pizza normal de tamaño" + tamano + " de " + tipo);
	}

	@Override
	public void hornearPizzaEspecial(String tamano, String tipo){
		//no hace nada
	}

}
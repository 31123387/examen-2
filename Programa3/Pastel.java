public class Pastel implements HornearPastel {
	HornearAdaptador hornearAdaptador;

	@Override
	public void hornear(String sabor, String tipo){
		if(tipo.equalsIgnoreCase("tres leches")){
        	System.out.println("Se horneo un pastel de sabor " + sabor + " de " + tipo);			
      	} 

      	else if(tipo.equalsIgnoreCase("normal") || tipo.equalsIgnoreCase("especial")){
         hornearAdaptador = new HornearAdaptador(tipo);
         hornearAdaptador.hornear(String sabor, String tipo);
      }
	}
}
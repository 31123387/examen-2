public class PizzaEspecial implements HornearPizza{

	@Override
	public void hornearPizzaNormal(String tamano, String tipo) {
		//no hace nada
	}

	@Override
	public void hornearPizzaEspecial(String tamano, String tipo){		
		System.out.println("Se horneo pizza especial de tamaño " + tamano + " de " + tipo);
	}

}
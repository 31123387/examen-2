public interface HornearPizza {
	public void hornearPizzaNormal(String tamano, String tipo);
	public void hornearPizzaEspecial(String tamano, String tipo);
}